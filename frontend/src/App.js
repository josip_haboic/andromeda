import React from 'react'
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom'
import { ApolloProvider } from 'react-apollo'


import './styles/css/index.css'
import { Nav } from './shared/components/layout/nav'
import { Home } from './pages/home'
import { Auth } from './pages/auth'
import { Dashboard } from './pages/dashboard'
import { Notifications } from './shared/components/notifications'
import { AuthProvider, AuthConsumer } from './shared/providers/auth'
import { NotificationProvider, NotificationsConsumer } from './shared/providers/notification'
import { PrivateRoute } from './shared/router/privateRoute'
import { apolloClient } from './shared/providers/apollo'





const appNavigation = (user) => {
  return [
    { url: '/', text: 'Home' },
    user && (user.emailVerified && { url: '/dashboard', text: 'Dashboard' }),
    user ?
      { url: '/auth/logout', text: 'Log Out' } :
      { url: '/auth/login', text: 'Log In' },
  ]
}

export const App = () => {
  return (
    <Router>
      <div className="container">
        <ApolloProvider client={apolloClient}>
          <NotificationProvider>
            <AuthProvider>
              <AuthConsumer>
                {({ user }) => {
                  return (
                    <Nav links={appNavigation(user)}></Nav>
                  )
                }}
              </AuthConsumer>

              <main>
                <Switch>
                  <Route path="/" exact component={Home} />
                  <Route path="/auth" component={Auth} />
                  <PrivateRoute path="/dashboard" component={Dashboard} />
                </Switch>
              </main>

            </AuthProvider>
            <NotificationsConsumer>
              {({ notifications, remove }) => {
                return (
                  <Notifications notifications={notifications} onRemove={remove} />
                )
              }}
            </NotificationsConsumer>
          </NotificationProvider>
        </ApolloProvider>
      </div>
    </Router >
  );
}
import React from 'react'



export const Home = (props) => {
    return (
        <div className="container">
            {props.children}
        </div>
    )
}
import React, { useState } from 'react'
import { NavLink } from 'react-router-dom'
import { ToggleButton } from '../../../shared/components/toggle/button'
import PropTypes from 'prop-types'



export const DashboardMenu = (props) => {
    const { items } = props
    const [menuVisible, setMenuVisible] = useState(true)

    return (
        <menu className="dashboard-menu">
            {menuVisible && items.map((item) => {
                return (
                    <NavLink
                        className="menu-item"
                        activeClassName="menu-item--active"
                        exact={item.exact && true}
                        to={item.url}
                        key={item.url}>
                        {item.text}
                    </NavLink >
                )
            })}
            <ToggleButton onClick={() => setMenuVisible(!menuVisible)} />
        </menu>
    )
}


DashboardMenu.propTypes = {
    items: PropTypes.array.isRequired
}

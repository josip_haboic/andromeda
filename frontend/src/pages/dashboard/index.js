import React from 'react'
import { Route, Switch } from 'react-router-dom'
import { DashboardMenu } from './components/menu'
import { Overview } from './modules/overview'
import { Employees } from './modules/employees'
import { Tools } from './modules/tools'
import { Vehicles } from './modules/vehicles'
import { Work } from './modules/work'
import { Storage } from './modules/storage'
import { Examples } from './modules/examples'
import { CalendarView } from './modules/calendar'




export const Dashboard = () => {
   
    const menuItems = [
        { url: '/dashboard', text: 'Dashboard', component: undefined, exact: true },
        { url: '/dashboard/employees', text: 'Employees', component: Employees },
        { url: '/dashboard/tools', text: 'Tools', component: Tools },
        { url: '/dashboard/vehicles', text: 'Vehicles', component: Vehicles },
        { url: '/dashboard/work', text: 'Work', component: Work },
        { url: '/dashboard/storage', text: 'Storage', component: Storage },
        { url: '/dashboard/examples', text: 'Examples', component: Examples },
        { url: '/dashboard/calendar', text: 'Calendar', component: CalendarView },
    ]


    return (
        <div className="dashboard">
            <DashboardMenu
                items={menuItems}>
            </DashboardMenu>

            <div className="dashboard-main">
                <Switch>
                    {menuItems.map((item) => {
                        if (item.component) {
                            return <Route path={item.url} exact component={item.component} key={item.url} />
                        }
                        return undefined
                    })}
                    <Overview></Overview>
                </Switch>
            </div>
        </div>
    )
}
import React from 'react'

import { Calendar } from '../../../shared/components/calendar'



export const CalendarView = () => {
    return (
        <div className="container">
            <Calendar />
        </div>
    )
}
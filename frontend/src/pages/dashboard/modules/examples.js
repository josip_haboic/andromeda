import React, { useState, useEffect } from 'react'

import { poly } from '../../../lib/generateData'
import { loremIpsum } from '../../../lib/const'
import { LineChart } from '../../../shared/components/chart/line'
import { BarChart } from '../../../shared/components/chart/bar'



export const Examples = () => {

    // fake data
    const [data, setData] = useState({
        0: [...poly(20)],
        1: [...poly(20)],
        2: [...poly(20)],
    })

    // width, height and margin of chart
    const width = 400
    const height = 200

    useEffect(() => setData({
        0: [...poly(20)],
        1: [...poly(20)],
        2: [...poly(20)],
    }), [])


    return (
        <React.Fragment>
            <section className="grid grid--2-col does-fade-in">

                <div className="h-320px has-y-scrollbar p-small">
                    <h2 className="is-white p-small">Report about something</h2>
                    <p className="is-white">{loremIpsum.medium}</p>
                    <p className="is-white">{loremIpsum.medium}</p>
                    <p className="is-white">{loremIpsum.short}</p>
                    <p className="is-white">{loremIpsum.medium}</p>
                    <p className="is-white">{loremIpsum.medium}</p>
                    <p className="is-white">{loremIpsum.medium}</p>
                    <p className="is-white">{loremIpsum.medium}</p>

                    <button onClick={() => setData({
                        0: [...poly(20)],
                        1: [...poly(20)],
                        2: [...poly(20)],
                    })}>
                        Randomize
                    </button>
                </div>

                <div className="chart">
                    <LineChart
                        width={width}
                        height={height}
                        data={{
                            items: [
                                {
                                    'data': data[0],
                                    'className': 'line-is-main'
                                },
                                {
                                    'data': [...poly(20)],
                                    'className': 'has-stroke-secondary'
                                },
                            ],
                        }}
                        scales={{
                            x: 'linear',
                            y: 'linear'
                        }}
                    >
                    </LineChart>
                </div>
            </section>

     

            <section className="grid grid--2-col does-fade-in">
                <div className="has-y-scrollbar h-320px p-small">
                    <h2 className="is-white p-small">Report about something else</h2>
                    <p className="is-white">{loremIpsum.medium}</p>
                    <p className="is-white">{loremIpsum.medium}</p>
                    <p className="is-white">{loremIpsum.short}</p>
                    <p className="is-white">{loremIpsum.medium}</p>
                    <p className="is-white">{loremIpsum.medium}</p>
                    <p className="is-white">{loremIpsum.medium}</p>
                    <p className="is-white">{loremIpsum.medium}</p>
                </div>

                <div className="chart">
                    <LineChart
                        width={width}
                        height={height}
                        data={{
                            items: [
                                { data: data[2] }
                            ]
                        }}
                        scales={{
                            x: 'linear',
                            y: 'linear'
                        }}
                    >
                    </LineChart>
                </div>
            </section>

    

            <section className="m-tb--large">

                <div className="stripe">
                    <p className="has-text-xsmall p-small">
                        Chart of 20 linear points - small version
                    </p>

                    <div className="chart chart--small">
                        <LineChart
                            width={width}
                            height={height}
                            data={{
                                items: [
                                    { data: [...poly(20)] }
                                ]
                            }}
                            dots={false}
                            grid={false}
                            xAxis={false}
                            yAxis={false}
                            scales={{
                                x: 'linear',
                                y: 'linear'
                            }}
                        />
                    </div>
                </div>
            </section>

            <section>
                <div className="is-vertical flex-center">
                    <div className="is-12 flex-center">
                        <div className="chart">
                            <BarChart
                                width={400}
                                height={200}
                                xAxis={false}
                                yAxis={true}
                                data={data[0]}>
                            </BarChart>
                        </div>
                    </div>
                    <div className="is-12 flex-center">
                        <button onClick={() => setData({
                            0: [...poly(20)],
                            1: [...poly(20)],
                            2: [...poly(20)],
                        })}>
                            Randomize
                    </button>
                    </div>
                </div>
            </section>

        </React.Fragment>
    )
}

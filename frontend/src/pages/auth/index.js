import React from 'react'
import { Route, Switch, Redirect } from 'react-router-dom'

import { LogIn } from './modules/login'
import { LogOut } from './modules/logout'
import { SignUp } from './modules/signup'



export const Auth = (props) => {
    return (
        <Switch>
            <Route path="/auth/login"  component={LogIn} />
            <Route path="/auth/logout" component={LogOut} />
            <Route path="/auth/signup" component={SignUp} />
            <Redirect to="/auth/signup" />
        </Switch>
    )
}
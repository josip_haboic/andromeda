import React from 'react'
import { Redirect } from 'react-router-dom'
import firebase from 'firebase'



export const LogOut = () => {
    firebase.auth().signOut()

    return (
        <Redirect to="/"></Redirect>
    )
}
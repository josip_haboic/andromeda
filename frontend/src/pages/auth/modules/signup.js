import React, { useState, useContext } from 'react'
import { Link, Redirect } from 'react-router-dom'
import firebase from 'firebase'

import { Form } from '../components/emailPasswordForm'
import { NotificationsContext } from '../../../shared/providers/notification'



export const SignUp = () => {
    const [email, setEmail] = useState('')
    const [password, setPassword] = useState('')
    const [payload, setPayload] = useState(undefined)
    const notify = useContext(NotificationsContext)

    const handleChange = (e) => {
        switch (e.target.id) {
            case 'email':
                setEmail(e.target.value)
                break
            case 'password':
                setPassword(e.target.value)
                break
            default: break
        }
    }

    const handleError = (error) => {
        console.error(error)
        notify.add({
            group: 'all',
            class: 'is-error',
            header: 'Error Occured',
            body: error.message
        })
    }

    const handleSubmit = (event) => {
        event.preventDefault()
        firebase.auth().createUserWithEmailAndPassword(email, password)
            .then((result) => {
                setPayload(result)
                result.user.sendEmailVerification({
                    url: process.env.REACT_APP_CONFIRMATION_EMAIL_REDIRECT,
                })
                notify.add({
                    group: 'all',
                    class: 'is-link',
                    header: 'Information',
                    body: 'You are now signed in, please check your e-mail to verify your account.'
                })

            })
            .catch(error => handleError(error))
    }

    return (
        <div className="hero does-fade-in">
            <div className="hero-header">
                <h1 className="has-text-white">Create Account</h1>
            </div>
            <div className="hero-body">
                <div className="box">
                    <Form
                        handleChange={handleChange}
                        handleSubmit={handleSubmit}>
                    </Form>
                </div>
            </div>
            <div className="hero-footer">
                <p className="has-text-black">
                    Already Signed Up? Then please - 
                    <Link to="/auth/login">
                        Log In
                    </Link>
                </p>
            </div>
            {payload ? <Redirect to="/" /> : undefined}
        </div>
    )
}
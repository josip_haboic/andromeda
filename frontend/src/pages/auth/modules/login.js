import React, { useState, useContext } from 'react'
import { Link, Redirect } from 'react-router-dom'
import firebase from 'firebase'

import { Form } from '../components/emailPasswordForm'
import { NotificationsContext } from '../../../shared/providers/notification'



export const LogIn = (props) => {
    const [email, setEmail] = useState(undefined)
    const [password, setPassword] = useState(undefined)
    const [payload, setPayload] = useState(undefined)
    const notify = useContext(NotificationsContext)

    const handleChange = (e) => {
        e.preventDefault()
        switch (e.target.id) {
            case 'email':
                setEmail(e.target.value)
                break
            case 'password':
                setPassword(e.target.value)
                break
            default: break
        }
    }
    const handleSubmit = async (e) => {
        e.preventDefault()
        firebase.auth().signInWithEmailAndPassword(email, password)
            .then((payload) => {
                notify.add({
                    group: 'all',
                    class: 'is-link',
                    header: 'Information',
                    body: 'You had successfully logged in.',
                    lifetime: 10000 // set lifetime so notification expires in 10 seconds
                })
                setPayload(payload)
            })
            .catch((error) => handleError(error))
    }

    const handleError = (error) => {
        console.error(error)
        notify.add({
            group: 'all',
            class: 'is-error',
            header: 'Error Occurred',
            body: error.message
        })
    }


    return (
        <div className="hero does-fade-in">
            <div className="hero-header">
                <h1 className="has-text-white">Log In</h1>
            </div>
            <div className="hero-body">
                <Form
                    handleChange={handleChange}
                    handleSubmit={handleSubmit}
                    handleError={handleError}
                >
                </Form>
            </div>
            <div className="hero-footer">
                <p className="has-text-black">
                    Not Signed Up? Then please - 
                    <Link to="/auth/signup">
                        Sign Up
                    </Link>
                </p>
            </div>
            {payload ? <Redirect to="/" /> : undefined}
            {props.children}
        </div>
    )
}
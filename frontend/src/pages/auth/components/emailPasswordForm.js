import React from 'react'
import PropTypes from 'prop-types'



export const Form = (props) => {
    const { handleSubmit, handleChange } = props

    return (
        <form
            onSubmit={handleSubmit}>

            <div className="is-horizontal flex-center m-tb--large">
                <div className="field is-mobile">
                    <div className="control">
                        <input
                            type="email"
                            id="email"
                            className="input"
                            onChange={handleChange}
                            placeholder="Email"
                            required
                            autoComplete="off"
                        />
                    </div>
                </div>

                <div className="field is-mobile">
                    <div className="control">
                        <input
                            type="password"
                            id="password"
                            className="input"
                            onChange={handleChange}
                            placeholder="Password"
                            required
                            autoComplete="off"
                        />
                    </div>
                </div>
            </div>

            <div className="field">
                <div className="control">
                    <button
                        className="is-link has-box-shadow--lift"
                        type="submit">
                        Submit
                     </button>
                </div>
            </div>

        </form>
    )
}


Form.propTypes = {
    handleSubmit: PropTypes.func.isRequired,
    handleChange: PropTypes.func.isRequired
}
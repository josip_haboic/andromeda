import React, { useContext } from 'react'
import { Route, Redirect } from 'react-router-dom'
import { AuthContext } from '../providers/auth'




export const PrivateRoute = ({component, ...rest}) => {
    const auth = useContext(AuthContext)
    if (auth.user && auth.user.emailVerified) {
        return (
            <Route
                {...rest}
                component={component}
            />
            )       
    }
    return (
        <Redirect to={{
            pathname: '/',
        }}/>
    )
}
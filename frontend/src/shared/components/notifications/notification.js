import React from 'react'
import PropTypes from 'prop-types'



export const Notification = (props) => {
    const { notification, onRemove } = props
    
    return (
        <div className="notification does-fade-in--slow" key={notification.id}>
            <div className="notification-header">
                <p className={`${notification.class}`}>{notification.header}</p>
                <button
                    className="is-bold"
                    onClick={() => {onRemove(notification.id)}}>
                    Dismiss
                </button>
            </div>
            <div className="notification-body">{notification.body}</div>
        </div>
    )
}

Notification.propTypes = {
    notification: PropTypes.object.isRequired,
    onRemove: PropTypes.func.isRequired
}
import React from 'react'
import { Notification } from './notification'
import PropTypes from 'prop-types'



export const Notifications = (props) => {
    const { notifications, onRemove } = props

    return (
        <div className="notifications">
            <div className="notification-list">
                {notifications && notifications.map((notification) => {
                    return (
                        <Notification notification={notification} onRemove={onRemove} key={notification.id}/>
                    )
                })}
            </div>
        </div>
    )
}

Notifications.propTypes = {
    notifications: PropTypes.array,
    onRemove: PropTypes.func.isRequired
}
import React, { useRef, useEffect } from 'react'
import PropTypes from 'prop-types'
import * as d3 from 'd3'



export const BarChart = (props) => {
    const {
        data,
        width,
        height,
        margin,
        children
    } = props

    const barChartRef = useRef(null)

    const svg = d3.select(barChartRef.current)

    const chart = svg.append('g')
        .attr('transform', `translate(${margin}, ${margin})`)

    const yScale = d3.scaleLinear()
        .range([height, 0])
        .domain([d3.min(data.map((item) => item.y)), d3.max(data.map((item) => item.y))])

    const xScale = d3.scaleBand()
        .range([0, width])
        .domain(data.map((item) => item.x))
        .padding(0.1)


    const bars = chart.selectAll('.bar')

    bars.data(data)
        .enter()
        .append('rect')
        .attr('class', 'bar')
        .attr('y', (d) => height)
        .attr('width', xScale.bandwidth())
        .attr('height', (d) => 0)
        .merge(bars)
        .attr('class', 'bar')
        .attr('x', (d) => xScale(d.x))
        .transition()
        .duration(2000)
        .attr('y', (d) => yScale(d.y))
        .attr('width', xScale.bandwidth())
        .attr('height', (d) => height - yScale(d.y))

    chart.append('g')
        .attr('class', 'chart-grid chart-grid--y')
        .attr('transform', `translate(0, 0)`)
        .style('stroke-dasharray', ('4', '4'))
        .call(d3.axisLeft(yScale).ticks(Math.round(data.length / 2)).tickSize(-width).tickFormat(''))

    chart.append('g')
        .attr('class', 'axis axis--y')
        .call(d3.axisLeft(yScale))
    chart.append('g')
        .attr('transform', `translate(0, ${height})`)
        .attr('class', 'axis axis--x')
        .call(d3.axisBottom(xScale))

    useEffect(() => {
        return () => {
            chart.selectAll('*')
                .remove()
        }
    })


    return (
        <svg ref={barChartRef} viewBox={[0, 0, width + margin * 2, height + margin * 2]}>
            <g>{children}</g>
        </svg>
    )
}


BarChart.defaultProps = {
    width: 400,
    height: 200,
    margin: 32
}

BarChart.propTypes = {
    data: PropTypes.array.isRequired,
    width: PropTypes.number,
    height: PropTypes.number,
    margin: PropTypes.number,
}

import React from 'react'
import PropTypes from 'prop-types'
import * as d3 from 'd3'

import { Axis } from './components/axis'
import { Line } from './components/line'
import { Grid } from './components/grid'
import { Dots } from './components/dots'



export const LineChart = (props) => {
    const {
        data,
        width,
        height,
        margin,
        xAxis,
        yAxis,
        grid,
        scales,
        dots,
        children
    } = props

    const fullWidth = width + margin * 2
    const fullHeight = height + margin * 2

    const n = d3.max(data.items.map((item) => {
        return item.data.length
    }))

    const xRange = [0, width]
    const yRange = [height, 0]

    const xDomainMin = d3.min(data.items.map((item) => {
        return d3.min(item.data.map((d) => d.x))
    }))

    const xDomainMax = d3.max(data.items.map((item) => {
        return d3.max(item.data.map((d) => d.x))
    }))

    const yDomainMin = d3.min(data.items.map((item) => {
        return d3.min(item.data.map((d) => d.y))
    }))

    const yDomainMax = d3.max(data.items.map((item) => {
        return d3.max(item.data.map((d) => d.y))
    }))

    const xDomain = [xDomainMin, xDomainMax]
    const yDomain = [yDomainMin, yDomainMax]

    const createScale = (type, domain, range) => {
        let scale
        switch (type) {
            case 'linear':
                scale = d3.scaleLinear()
                break
            case 'time':
                scale = d3.scaleTime()
                break
            default:
                scale = d3.scaleLinear()
                break

        }

        return scale.domain(domain).range(range)
    }

    const xScale = createScale(scales.x, xDomain, xRange)
    const yScale = createScale(scales.y, yDomain, yRange)


    return (
        <svg viewBox={[0, 0, fullWidth, fullHeight]}>

            {xAxis &&
            <Axis
                scale={xScale}
                margin={margin}
                name={"x"}
                ticks={Math.round(n / 2)}
                height={height}
                position={"bottom"}
            />}

            {yAxis &&
            <Axis
                scale={yScale}
                margin={margin}
                name={"y"}
                ticks={Math.round(n / 2)}
                height={0}
                position={"left"}
            />}

            {grid &&
                <Grid
                    ticks={Math.round(n / 2)}
                    xScale={xScale}
                    yScale={yScale}
                    width={width}
                    margin={margin}
                    height={height}
                />
            }

            {data.items.map((item, index) => {
                return (
                    <Line
                        key={index}
                        data={item.data.map((d) =>({x: xScale(d.x) + margin, y: yScale(d.y) + margin}))}
                    />
                )
            })}

            <g>
                {children}
            </g>


            {dots &&
                <Dots
                    data={data.items[0].data}
                    xScale={xScale}
                    yScale={yScale}
                    margin={margin}
                    radius={4}
                />}



        </svg>
    )
}

LineChart.defaultProps = {
    width: 320,
    height: 100,
    margin: 32,
    xAxis: true,
    yAxis: true,
    grid: true,
    dots: false
}


LineChart.propTypes = {
    data: PropTypes.object.isRequired,
    width: PropTypes.number,
    height: PropTypes.number,
    margin: PropTypes.number,
    xAxis: PropTypes.bool,
    yAxis: PropTypes.bool,
    grid: PropTypes.oneOfType([PropTypes.object, PropTypes.bool]),
    dots: PropTypes.oneOfType([PropTypes.object, PropTypes.bool]),
    scales: PropTypes.object,
}
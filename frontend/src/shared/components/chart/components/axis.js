import React, { useRef } from 'react'
import * as d3 from 'd3';
import PropTypes from 'prop-types'



export const Axis = (props) => {
    const { scale, name, ticks, position, margin, height } = props
    const axisRef = useRef(null)

    const axis = d3.select(axisRef.current)
        .attr('class', `axis axis-${name}`)
        .attr('transform', `translate(${margin}, ${height + margin})`)

    switch (position) {
        case 'top':
            axis.call(d3.axisTop(scale).ticks(ticks))
            break
        case 'bottom':
            axis.call(d3.axisBottom(scale).ticks(ticks))
            break
        case 'left':
            axis.call(d3.axisLeft(scale).ticks(ticks))
            break
        case 'right':
            axis.call(d3.axisRight(scale).ticks(ticks))
            break
        default:
            break

    }

    return (
        <g ref={axisRef}></g>
    )
}



Axis.defaultProps = {
    ticks: 6
}


Axis.propTypes = {
    scale: PropTypes.func.isRequired,
    margin: PropTypes.number.isRequired,
    name: PropTypes.string.isRequired,
    ticks: PropTypes.number.isRequired,
    height: PropTypes.number.isRequired,
    position: PropTypes.string.isRequired,
}
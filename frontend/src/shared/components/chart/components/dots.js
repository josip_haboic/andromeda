import React, { useRef } from 'react'
import * as d3 from 'd3'
import PropTypes from 'prop-types'



export const Dots = (props) => {
    const { data, xScale, yScale, margin, radius } = props
    const dotsRef = useRef(null)


    d3.select(dotsRef.current).selectAll('.dot').remove().exit()

    d3.select(dotsRef.current).selectAll('.dot')
        .data(!data.isArray ? data : data[0])
        .enter()
        .append('circle')
        .attr('class', 'dot')
        .attr('cx', (d, _i) => xScale(d.x) + margin)
        .attr('cy', (d, _i) => yScale(d.y) + margin)
        .attr('r', radius)
        .transition().attr('class', 'dot dot--active does-fade-in--xslow')


    return (
        <g ref={dotsRef}></g>
    )
}


Dots.propTypes = {
    data: PropTypes.oneOfType([PropTypes.array, PropTypes.object]).isRequired,
    xScale: PropTypes.func.isRequired,
    yScale: PropTypes.func.isRequired,
    margin: PropTypes.number.isRequired,
    radius: PropTypes.number.isRequired,
}
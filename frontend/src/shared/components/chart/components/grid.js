import React, { useRef } from 'react'
import * as d3 from 'd3'
import PropTypes from 'prop-types'



export const Grid = (props) => {
    const { xScale, yScale, height, width, ticks, margin } = props
    const xLines = useRef(null)
    const yLines = useRef(null)

    const gridX = d3.select(xLines.current)
        gridX.attr('class', 'chart-grid chart-grid--x')
        gridX.attr('transform', `translate(${margin}, ${height + margin})`)
        gridX.style('stroke-dasharray', ('3, 3'))
        gridX.call(d3.axisBottom(xScale).ticks(ticks).tickSize(-height).tickFormat(''))

        const gridY = d3.select(yLines.current)
        gridY.attr('class', 'chart-grid chart-grid--y')
        gridY.attr('transform', `translate(${margin}, ${margin})`)
        gridY.style('stroke-dasharray', ('3, 3'))
        gridY.call(d3.axisLeft(yScale).ticks(ticks).tickSize(-width).tickFormat(''))

    return (
        <React.Fragment>
            <g ref={xLines}></g>
            <g ref={yLines}></g>
        </React.Fragment>
    )
}



Grid.defaultProps = {
    ticks: 6
}

Grid.propTypes = {
    margin: PropTypes.number.isRequired,
    ticks: PropTypes.number.isRequired,
    xScale: PropTypes.func.isRequired,
    yScale: PropTypes.func.isRequired,
    height: PropTypes.number.isRequired,
    width: PropTypes.number.isRequired,
}
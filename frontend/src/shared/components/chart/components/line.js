import React, { useRef } from 'react'
import * as d3 from 'd3'
import PropTypes from 'prop-types'



export const Line = (props) => {
    const { data, color, width } = props
    const pathRef = useRef(null)
    
    const line = d3.line()
        .x((d) => d.x)
        .y((d) => d.y)
        .curve(d3.curveMonotoneX)
    
    d3.select(pathRef.current)
        .datum(data)
        .transition()
        .duration(700)
        .attr('class', 'line')
        .attr('stroke', color)
        .attr('stroke-width', width)
        .attr('d', line)

    return (
        <path ref={pathRef}></path>
    )
}



Line.defaultProps = {
    color: '#222222',
    width: 2
}


Line.propTypes = {
    data: PropTypes.array.isRequired,
    color: PropTypes.string,
    width: PropTypes.number
}
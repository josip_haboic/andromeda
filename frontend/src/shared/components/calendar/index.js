import React, { useState } from 'react'
import * as dateFns from 'date-fns'



export const Calendar = (props) => {

    const [currentMonth, setCurrentMonth] = useState(new Date())
    const [selectedDate, setSelectedDate] = useState(new Date())
    const currentDate = new Date()

    const header = () => {
        const dateFormat = 'MMMM YYYY'
        return (
            <div className="header">
                <div className="header-bar">
                    <div className="is-2">
                        <div
                            className="icon arrow arrow-left change-month is-not-selectable"
                            onClick={previousMonth}>
                            <svg
                                version="1.1"
                                xmlns="http://www.w3.org/2000/svg"
                                xmlnsXlink="http://www.w3.org/1999/xlink"
                                x="0px"
                                y="0px"  
	                            viewBox={[0, 0, 492, 492]}>
                                <g>
	                                <g>
		                                <path d="M464.344,207.418l0.768,0.168H135.888l103.496-103.724c5.068-5.064,7.848-11.924,7.848-19.124
			                                c0-7.2-2.78-14.012-7.848-19.088L223.28,49.538c-5.064-5.064-11.812-7.864-19.008-7.864c-7.2,0-13.952,2.78-19.016,7.844
			                                L7.844,226.914C2.76,231.998-0.02,238.77,0,245.974c-0.02,7.244,2.76,14.02,7.844,19.096l177.412,177.412
			                                c5.064,5.06,11.812,7.844,19.016,7.844c7.196,0,13.944-2.788,19.008-7.844l16.104-16.112c5.068-5.056,7.848-11.808,7.848-19.008
			                                c0-7.196-2.78-13.592-7.848-18.652L134.72,284.406h329.992c14.828,0,27.288-12.78,27.288-27.6v-22.788
			                                C492,219.198,479.172,207.418,464.344,207.418z"
                                        />
	                                </g>
                                </g>
                            </svg>
                        </div>
                    </div>
                    <div className="is-8 current-month">
                        <p className="is-not-selectable">
                            {dateFns.format(currentMonth, dateFormat)}
                        </p>
                    </div>
                    <div className="is-2">
                        <div
                            className="icon arrow arrow-right change-month is-not-selectable"
                            onClick={nextMonth}>
                            <svg
                                version="1.1"
                                xmlns="http://www.w3.org/2000/svg"
                                xmlnsXlink="http://www.w3.org/1999/xlink"
                                x="0px"
                                y="0px"  
	                            viewBox={[0, 0, 492, 492]}>
                                <g>
	                                <g>
		                                <path d="M464.344,207.418l0.768,0.168H135.888l103.496-103.724c5.068-5.064,7.848-11.924,7.848-19.124
			                                c0-7.2-2.78-14.012-7.848-19.088L223.28,49.538c-5.064-5.064-11.812-7.864-19.008-7.864c-7.2,0-13.952,2.78-19.016,7.844
			                                L7.844,226.914C2.76,231.998-0.02,238.77,0,245.974c-0.02,7.244,2.76,14.02,7.844,19.096l177.412,177.412
			                                c5.064,5.06,11.812,7.844,19.016,7.844c7.196,0,13.944-2.788,19.008-7.844l16.104-16.112c5.068-5.056,7.848-11.808,7.848-19.008
			                                c0-7.196-2.78-13.592-7.848-18.652L134.72,284.406h329.992c14.828,0,27.288-12.78,27.288-27.6v-22.788
			                                C492,219.198,479.172,207.418,464.344,207.418z"
                                        />
	                                </g>
                                </g>
                            </svg>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
    const days = () => {
        const dateFormat = "ddd"
        const days = []

        let startDate = dateFns.startOfWeek(currentMonth)

        for (let i = 0; i < 7; i += 1) {
            days.push(
                <div className="day is-not-selectable" key={i}>
                    {dateFns.format(dateFns.addDays(startDate, i), dateFormat)}
                </div>
            )
        }

        return (
            <div className="days-names">
                {days}
            </div>
        )
    }

    const cells = () => {
        const monthStart = dateFns.startOfMonth(currentMonth)
        const monthEnd = dateFns.endOfMonth(currentMonth)
        const startDate = dateFns.startOfWeek(monthStart)
        const endDate = dateFns.endOfWeek(monthEnd)

        const dateFormat = "D"
        const rows = []

        let days = []
        let day = startDate
        let formattedDate = ""

        while (day <= endDate) {
            for (let i = 0; i < 7; i += 1) {
                formattedDate = dateFns.format(day, dateFormat)
                const cloneDay = day
                days.push(
                    <div className={`cell ${
                        !dateFns.isSameMonth(day, monthStart)
                            ? "disabled"
                            : dateFns.isSameDay(day, selectedDate)
                                ? "selected" 
                                : dateFns.isSameDay(day, currentDate)
                                    ? "current"
                                    : ""
                        }`}
                        key={day}
                        onClick={() => onDateClick(dateFns.parse(cloneDay))}
                    >
                        {/* <span className="number">{formattedDate}</span> */}
                        <span className="bg is-not-selectable">{formattedDate}</span>
                    </div>
                )

                day = dateFns.addDays(day, 1)
            }

            rows.push(
                <div className="days" key={day}>
                    {days}
                </div>
            )

            days = []
        }

        return (
            <div className="body">
                {rows}
            </div>
        )
    }

    const onDateClick = (day) => {
        setSelectedDate(day)
    }

    const nextMonth = () => {
        setCurrentMonth(dateFns.addMonths(currentMonth, 1))
    }
    const previousMonth = () => {
        setCurrentMonth(dateFns.subMonths(currentMonth, 1))
    }

    return (
        <div className="calendar">
            {header()}
            {days()}
            {cells()}
        </div>
    )
}
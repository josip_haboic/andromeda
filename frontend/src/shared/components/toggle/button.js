import React, { useState } from 'react'
import PropTypes from 'prop-types'



export const ToggleButton = (props) => {
    const { onClick, children } = props
    const [closed, setClosed] = useState(false)

    return (
        <button 
            className={`toggle-button${closed ? ' toggle-button--closed' : ''}`} 
            onClick={(e) =>{
                setClosed(!closed)
                onClick(e)
            }}>{ children || <React.Fragment>
                <div>•</div>
                <div>•</div>
                <div>•</div>
            </React.Fragment>}
        </button>
    )
}



ToggleButton.propTypes = {
    onClick: PropTypes.func.isRequired
}
import React from 'react'
import PropTypes from 'prop-types'



export const NavIcon = (props) => {
    const { onClick } = props

    return (
        <div className="nav-icon" onClick={onClick}>
            <div className="nav-icon-bar"></div>
            <div className="nav-icon-bar"></div>
            <div className="nav-icon-bar"></div>
        </div>
    )
}

NavIcon.propTypes = {
    onClick: PropTypes.func.isRequired
}
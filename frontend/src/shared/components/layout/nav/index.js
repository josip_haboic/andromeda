import React, { useState } from 'react'
import { NavLink } from 'react-router-dom'
import { NavIcon } from './icon'
import PropTypes from 'prop-types'



export const Nav = (props) => {
    const { links } = props
    const [collapsed, setCollapsed] = useState(false)
    const onToggle = (_event) => {
        setCollapsed(!collapsed)
    }

    return (
        <nav role="navigation" aria-label="main navigation">
            <div className="logo">
                Andromeda
            </div>

            <div className={collapsed ? "nav-items is-hidden--mobile" : "nav-items"}>
                {links.filter((link) => !!link).map(
                    (link) => (
                        <NavLink
                            to={link.url}
                            key={link.url}
                            className="nav-item"
                            activeClassName="nav-item--active"
                            exact>
                            {link.text}
                        </NavLink>
                    )
                )}
            </div>

            <div className="nav-icon">
                <NavIcon onClick={onToggle} />
            </div>
        </nav>
    )
}

Nav.propTypes = {
    links: PropTypes.array
}
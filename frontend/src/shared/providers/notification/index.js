import React, { useState } from 'react'

import { uuid8 } from '../../../lib/uuid'



export const NotificationsContext = React.createContext()


export const NotificationProvider = ({ children }) => {
    const [state, setState] = useState({
        notifications: []
    })

    const assignId = (notification) => ({...notification, id: uuid8()})

    const add = (notification) => setState(
        (prevState) => {
            const notificationWithId = assignId(notification)
            notification.lifetime && setTimeout(() => remove(notificationWithId.id), notification.lifetime)
            return { notifications: [...prevState.notifications, notificationWithId] }
        }
    )
    const remove = (id) => setState(
        (prevState) => ({notifications: prevState.notifications.filter((item) => item.id !== id)})
    )

    return (
        <NotificationsContext.Provider value={{
            notifications: state.notifications,
            add,
            remove,
        }}>{ children }
        </NotificationsContext.Provider>
    )
}


export const NotificationsConsumer = NotificationsContext.Consumer;
import { ApolloClient } from 'apollo-boost'
import { InMemoryCache } from 'apollo-cache-inmemory'
import { HttpLink } from 'apollo-link-http'
import firebase from 'firebase'


const getToken = async () => {
    const user = firebase.auth().currentUser
    if (user) {
        try {
            const idToken = await user.getIdToken(/* forceRefresh */ true)
            console.log(idToken)
            return idToken

        } catch(error) {
            // Handle error
            console.error(error)
        }
    }

}

export const apolloClient = new ApolloClient({
    link: new HttpLink({uri: 'http://localhost:4000/graphql'}),
    cache: new InMemoryCache(),
    headers: { authorization: getToken() },

})
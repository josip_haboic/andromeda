export const random = (n=10, max=10) => {
    const values = []
    while (n !== 0) {
      values.push(Math.random() * max)
      n -= 1
    }
    return values.sort((a, b) => a >= b).map((x, i) => {
      return { x: i, y: x }
    }
  )
}

export function * sinWave(n) {
  let x = 0
  const step = 1
  while (n > 0) {
    yield {x: x, y: Math.sin(x)}
    x += step
    n -= 1
  }
}

export function * linear(n) {
  const a = Math.random() * (Math.random() > 0.5 ? -1 : 1)
  const b =  Math.random() * (Math.random() > 0.5 ? -1 : 1)
  const step = 1
  let x = 0
  while (n > 0) {
    yield {x: x, y: a * x + b}
    n -= 1
    x += step
  }
}

export function * cubic(n) {
  const a = Math.random() * (Math.random() > 0.5 ? -1 : 1)
  const b = Math.random() * (Math.random() > 0.5 ? -1 : 1)
  const c = Math.random() * (Math.random() > 0.5 ? -1 : 1)
  const d = Math.random() * (Math.random() > 0.5 ? -1 : 1)
  let x = 0
  const step = 1

  while (n > 0) {
    yield {x: x, y: a*x*x*x + b*x*x + c*x + d}
    x += step
    n -= 1
  }
}

export function * poly(n) {
  const a = Math.random() * (Math.random() > 0.5 ? -1 : 1)
  const b = Math.random() * (Math.random() > 0.5 ? -1 : 1)
  const c = Math.random() * (Math.random() > 0.5 ? -1 : 1)
  const d = Math.random() * (Math.random() > 0.5 ? -1 : 1)
  const e = Math.random() * (Math.random() > 0.5 ? -1 : 1)
  let x = 0
  const step = 1

  while (n > 0) {
    yield {x: x, y: a*b*c*d*x*x + c*x + d - x + e}
    x += step
    n -= 1
  }
}
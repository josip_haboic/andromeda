export const loremIpsum = {
    short: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla ligula nisi, vulputate eu ornare eu, aliquet non sem.',
    medium: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla ligula nisi, vulputate eu ornare eu, aliquet non sem. Suspendisse lectus tellus, rutrum non venenatis a, pulvinar ac urna. Integer tincidunt imperdiet urna in mollis. Sed feugiat imperdiet diam. Quisque dapibus sapien sed nunc suscipit maximus. Vivamus laoreet iaculis nisi, vel porta nulla faucibus sit amet.'
}
import { createConnection } from 'typeorm'



export const connectDatabase = async () => {
  return createConnection({
    type: 'sqlite',
    database: './storage/database.db',
    entities: ['./src/orm/entities/*.ts'],
    subscribers: ['./src/orm/subscribers/*.ts'],
    logging: [
      'query',
      'error'
    ],
    cache: true,
    synchronize: true,
  }).then((connection: any) => {
    return connection
  })
}

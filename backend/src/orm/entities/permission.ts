import {
  BaseEntity,
  BeforeInsert,
  Column,
  Entity,
  OneToOne,
  PrimaryGeneratedColumn,
} from 'typeorm'
import { Role } from './role'
import { uuid8 } from '../../lib/uuid'


@Entity('permissions')
export class Permission extends BaseEntity {

  @PrimaryGeneratedColumn('uuid')
  public id: string

  @Column({type: 'int', default: 0, nullable: false})
  public level: number

  @OneToOne(_type => Role, (role: Role) => role.permission)
  public role: Role

  @BeforeInsert()
  public initialize() {
    this.id = uuid8()
  }
}

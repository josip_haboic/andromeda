import * as bcrypt from 'bcrypt'
import {
  BaseEntity,
  BeforeInsert,
  Column,
  CreateDateColumn,
  Entity,
  JoinColumn,
  OneToOne,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm'
import { uuid8 } from '../../lib/uuid'
import { Profile } from './profile'
// import { Role } from './role';


@Entity('users')
export class User extends BaseEntity {

  @PrimaryGeneratedColumn('uuid')
  public id: string

  @CreateDateColumn()
  public createdAt: Date

  @UpdateDateColumn()
  public updatedAt: Date

  @Column({type: 'text', nullable: false, unique: true})
  public name: string

  @Column({type: 'text', nullable: false, length: 255})
  public password: string

  @Column({type: 'text', nullable: false, unique: true})
  public email: string

  @Column({type: 'boolean', default: false, nullable: false})
  public confirmed: boolean

  @OneToOne(_type => Profile, (profile: Profile) => profile.user, {cascade: true})
  @JoinColumn()
  public profile: Profile

  @BeforeInsert()
  public async initialize() {
    this.id = uuid8()
    this.password = await bcrypt.hash(this.password, 10)
  }
}

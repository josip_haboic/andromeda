import {
    BaseEntity,
    BeforeInsert,
    Column,
    Entity,
    OneToOne,
    PrimaryGeneratedColumn
} from 'typeorm'
import { User } from './user'
import { uuid8 } from '../../lib/uuid'


@Entity('profiles')
export class Profile extends BaseEntity {

  @PrimaryGeneratedColumn('uuid')
  public id: string

  @Column({type: 'text', nullable: true})
  public gender: string

  @Column({type: 'text', nullable: true})
  public photo: string

  @OneToOne(_type => User, (user: User) => user.profile)
  public user: User

  @BeforeInsert()
  public async initialize() {
    this.id = uuid8()
  }
}

import {
  BaseEntity,
  BeforeInsert,
  Column,
  Entity,
  JoinColumn,
  OneToOne,
  PrimaryGeneratedColumn,
} from 'typeorm'
import { Permission } from './permission'
import { User } from './user'
import { uuid8 } from '../../lib/uuid'


@Entity('roles')
export class Role extends BaseEntity {

  @PrimaryGeneratedColumn('uuid')
  public id: string

  @Column({type: 'text', nullable: false})
  public name: string

  @OneToOne(_type => User, (user: User) => user.profile)
  public user: User

  @OneToOne(_type => Permission, (permission: Permission) => permission.role)
  public role: Role

  @OneToOne(_type => Permission, (permission: Permission) => permission.role)
  @JoinColumn()
  public permission: Permission

  @BeforeInsert()
  public async initialize() {
    this.id = uuid8()
  }
}

import { ApolloServer } from 'apollo-server-koa'

import { ApolloModules } from './modules'
import { auth } from '../firebase'



// create apollo server and add it to application
export const connectGraphQL = (app: any) => {
  const apollo = new ApolloServer(
    {
      modules: ApolloModules,
      context: ({ ctx }) => {
        let user = null
        try {
          const token = ctx.req.headers.authorization.replace('Bearer ', '')
          user = auth().verifyIdToken(token)
        } catch (error) {
          // tslint:disable-next-line
          console.error(error)
        }

        return user
      }
    }
  )
  apollo.applyMiddleware({app})

  return apollo
}



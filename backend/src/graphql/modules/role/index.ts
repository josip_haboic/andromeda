import { typeDefs } from './typedefs'
import { resolvers } from './resolvers'


export const RoleModule = {
  typeDefs,
  resolvers
}

import gql from 'graphql-tag'



export const typeDefs = gql`
  type Role {
    id: String
    name: String
    permission: [Permission]
  }

  extend type Query {
    role(id: String!): Role
    roles: [Role]
  }
`

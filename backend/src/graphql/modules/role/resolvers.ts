import { Role } from '../../../orm/entities/role'


export const resolvers = {
  Query: {
    role: (_: any, {id}) => {
      return Role.find(id)
    }
  }
}

import { UserModule } from './user'
import { ProfileModule } from './profile'
import { RoleModule } from './role'
import { PermissionModule } from './permission'


// Export modules we want to use
export const ApolloModules = [
  UserModule,
  ProfileModule,
  RoleModule,
  PermissionModule,
]

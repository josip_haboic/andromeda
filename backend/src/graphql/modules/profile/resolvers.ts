import { Profile } from '../../../orm/entities/profile'
import { User } from '../../../orm/entities/user'



export const resolvers = {
  Query: {
    profile: ({ id }) => {
      return Profile.find(id)
    },
    profiles: () => {
      return Profile.find()
    }
  },
  Mutation: {
    createProfile: async (_: any, { userId }) => {
      const user = await User.findOne(userId, {relations: ['profile']})

      user.profile = new Profile()
      await user.profile.save()

      return user.profile
    }
  },
  Profile: {
    user: async (parent: any) => {
      return User.findOne(parent.userId, {relations: ['profile']})
    }
  }
}

import gql from 'graphql-tag'


export const typeDefs = gql`
  type Profile {
    id: ID!
    gender: String
    photo: String
    user: User!
  }

  extend type Query {
    profile(id: ID!): Profile
    profiles: [Profile]!
  }

  extend type Mutation {
    createProfile(userId: ID): Profile!
  }
`

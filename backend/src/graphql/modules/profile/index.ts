import { typeDefs } from './typedefs'
import { resolvers } from './resolvers'


export const ProfileModule = {
  typeDefs,
  resolvers
}

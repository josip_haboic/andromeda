import { Permission } from '../../../orm/entities/permission'


export const resolvers = {
  Query: {
    permission: (_, {id}) => {
      return Permission.find(id)
    }
  }
}

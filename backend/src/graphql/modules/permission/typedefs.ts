import gql from 'graphql-tag'


export const typeDefs = gql`
  type Permission {
    id: String!
    type: String!
  }

  extend type Query {
    permission(id: String): Permission
    permissions: [Permission]
  }
`

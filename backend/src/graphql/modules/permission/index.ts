import { typeDefs } from './typedefs'
import { resolvers } from './resolvers'


export const PermissionModule = {
  typeDefs,
  resolvers
}

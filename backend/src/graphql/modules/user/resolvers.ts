import { Profile } from '../../../orm/entities/profile'
// import { Role } from '../../../orm/entities/role'
import { User } from '../../../orm/entities/user'


export const resolvers = {
  Query: {
    user: async ({ id }) => {
      return User.find({id})
    },
    users: async () => {
      return User.find()
    },
  },
  Mutation: {
    createUser: async (_: any, { name, password, email }) => {
        const user = new User()
        user.name = name
        user.password = password
        user.email = email
        await user.save()

        return user
    }
  },
  User: {
    profile: async(parent: any) => {
      return Profile.findOne(parent.profileId, {relations: ['user']})
    }
  }
}

import gql from 'graphql-tag'


export const typeDefs = gql`
  type User {
    id: String!
    name: String!
    password: String!
    email: String!
    confirmed: Boolean!
    createdAt: String!
    updatedAt: String!
    profile: Profile
    # role: Role
  }

  extend type Query {
    user(id: String!): User
    users: [User]!
  }

  extend type Mutation {
    createUser(name: String, password: String, email: String): User!
  }
`

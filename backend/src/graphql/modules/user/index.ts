import { typeDefs } from './typedefs'
import { resolvers } from './resolvers'


export const UserModule = {
  typeDefs,
  resolvers
}

import * as admin from 'firebase-admin'


// https://firebase.google.com/docs/admin/setup



admin.initializeApp({
  credential: admin.credential.cert('./firebase.json'),
  databaseURL: process.env.FIREBASE_SDK_DATABASE_URL
});

export const auth = admin.auth
export const database = admin.database

import * as Koa from 'koa'
import * as bodyParser from 'koa-bodyparser'
import * as cors from 'koa2-cors'
import * as logger from 'koa-logger'
import * as session from 'koa-session'
import * as dotenv from 'dotenv'
import { connectDatabase } from './orm'
import { connectGraphQL } from './graphql'
import { router } from './router'



dotenv.config()



const app = new Koa()
app.keys = [process.env.SECRET_KEY]
app.use(logger())
app.use(bodyParser())
app.use(session({}, app))
app.use(cors({
  origin: (ctx) => {
    if (ctx.url === '/private') {
      return false;
    }

    return 'http://localhost:3000';
  },
  exposeHeaders: ['WWW-Authenticate', 'Server-Authorization', 'Authorization'],
  maxAge: 586400, // 24 hours
  credentials: true,
  allowMethods: ['GET', 'POST', 'HEAD'],
  allowHeaders: ['Content-Type', 'Authorization', 'Accept'],
}))
app.use(router.routes()).use(router.allowedMethods())


// connect to the database using TypeORM
connectDatabase()
// tslint:disable-next-line
console.log('Connection to database established\n')


// connect apollo server to this application
connectGraphQL(app)
// tslint:disable-next-line
console.log('Apollo GraphQL server running on: http://localhost:4000/graphql\n')


// finnaly start application
app.listen({port: 4000}, () => {
  // tslint:disable-next-line
  console.log(`Application Server running on: http://localhost:4000\n`)
})

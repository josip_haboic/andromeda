import * as fs from 'fs'


const deleteDatabase = async (path: string) => {
    fs.unlink(path, (error: Error) => {
      // tslint:disable-next-line
      console.error(error)
    })
    // tslint:disable-next-line
    console.log('Database deleted')
}

deleteDatabase('./storage/database.db')
